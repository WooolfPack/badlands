import base64
import logging
import json
import boto3


def lambda_handler(event, context):
    """
    AWS Lambda function.
    Exposed via URL.
    Sensor data caching.
    """
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    # Add the authorization header if not set (default to an empty string)
    event['headers']['authorization'] = event['headers'].get('authorization', [{'key': 'authorization', 'value': ''}])

    # This is the expecteded authorization header
    expected_authorization = 'Basic ' + base64.b64encode("xxxyyyzzz".encode()).decode()
    
    # Test if the authorization header is not matching (401 response in this case)
    if event['headers']['authorization'] != expected_authorization:
        logger.info(f"Unauthorized") 
        logger.info(f"event: {event}") 
        return {
            'status': '401',
            'statusDescription': 'Unauthorized',
            'body': 'Unauthorized',
            'headers': {
                'www-authenticate': [{'key': 'WWW-Authenticate', 'value': 'Basic'}]
            }
        }
    else:
        ssm = boto3.client('ssm')
        event_body = json.loads(event["body"])
        pm10 = event_body["sensordatavalues"][0]["value"]
        pm25 = event_body["sensordatavalues"][1]["value"]
        temp_out = event_body["sensordatavalues"][7]["value"]
        logger.info(f"event: {pm10, pm25, temp_out}") 

        ssm.put_parameter(
            Name='temp_out_divin',
            Value=str(temp_out),
            Type='String',
            Overwrite=True)
        ssm.put_parameter(
            Name='pm10_out_divin',
            Value=str(pm10),
            Type='String',
            Overwrite=True)
        ssm.put_parameter(
            Name='pm25_out_divin',
            Value=str(pm25),
            Type='String',
            Overwrite=True)

        # The authorization header is matching, forward the request
        return {"stat": 1}
