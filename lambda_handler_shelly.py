import urllib3
from urllib.parse import urlencode
import json
import logging
import time
import datetime
import boto3


def lambda_handler(event, context):
    """
    AWS Lambda function.
    Running periodically every 3 minutes.
    Ventilation automation.
    """

    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    http = urllib3.PoolManager()

    pm10_limit_on = 10.0
    pm25_limit_on = 5.0
    pm10_limit_off = 17.0
    pm25_limit_off = 8.5

    shelly_switch_url = 'https://shelly-25-eu.shelly.cloud/device/relay/control/?'
    shelly_status_url = 'https://shelly-25-eu.shelly.cloud/device/status/?'
    shelly_settings_url = 'https://shelly-25-eu.shelly.cloud/device/settings/?'
    shelly_timeout = 3
    auth = {"auth_key": "xyz",
            "id": "xyz"}

    def switch_handler(response):
        """
        Handler for Shelly switch relay api call response.
        :param response: response from Shelly api /device/relay/control
        """
        try:
            response_text = json.loads(response.data.decode('utf-8'))
            if not response_text["isok"]:
                logger.error("Shelly API switch - not OK response.")
                raise Exception("Shelly API switch - not OK response.")
        except:
            logger.error("Shelly API switch - malformed response.")
            raise

    def fallback():
        """
        Fallback scenario - turn vent off.
        """
        try:
            logger.warning("Fallback scenario triggered.")
            # turn shelly off
            payload = {}
            payload["channel"] = "0"
            payload["turn"] = "off"
            time.sleep(2)
            call_api(shelly_switch_url, shelly_timeout, switch_handler, post=True, fields=payload, auth=auth)
        except:
            logger.exception("Fallback exception.")
            raise

    def get_luftdaten_pm(response):
        """
        Handler for Luftdaten api call response.
        :param response: response from Luftdaten api
        :return: pm
        """
        try:
            response_text = json.loads(response.data.decode('utf-8'))
            if len(response_text) != 0:
                pm10 = list()
                pm10.append(float(response_text[0]["sensordatavalues"][0]["value"]))
                try:
                    pm10.append(float(response_text[1]["sensordatavalues"][0]["value"]))
                except IndexError:
                    logger.warning("2nd PM10 value NA.")

                pm25 = list()
                pm25.append(float(response_text[0]["sensordatavalues"][1]["value"]))
                try:
                    pm25.append(float(response_text[1]["sensordatavalues"][1]["value"]))
                except IndexError:
                    logger.warning("2nd PM2.5 value NA.")

                # logger.info("PM10: %s" % pm10)
                # logger.info("PM2.5: %s" % pm25)

                return pm10, pm25
            else:
                logger.error("Empty response returned.")
                raise Exception("Empty response returned.")
        except:
            logger.error("Luftdaten API - malformed response.")
            raise

    def get_shelly_status(response):
        """
        Handler for Shelly status api call response.
        :param response: response from Shelly api endpoint /device/status
        :return: is on state (bool)
        """
        try:
            response_text = json.loads(response.data.decode('utf-8'))
            if response_text["isok"] is True:
                ison = response_text["data"]["device_status"]["relays"][0]["ison"]
                time_now = datetime.datetime.now()
                logger.info("Shelly status: %s" % str(ison))
                return ison, time_now
            else:
                logger.error("Shelly API status - not OK response.")
                raise Exception("Shelly API status - not OK response.")
        except:
            logger.error("Shelly API status - malformed response.")
            raise
        
    def get_shelly_settings(response):
        """
        Handler for Shelly status api call response.
        :param response: response from Shelly api endpoint /device/status
        :return: scheduler bool, and scheduler_on, scheduler_off strings (times)
        """
        try:
            response_text = json.loads(response.data.decode('utf-8'))
            if response_text["isok"] is True:
                scheduler_on = None
                scheduler_off = None
                scheduler = response_text['data']['device_settings']['relays'][0]['schedule']
                if scheduler:
                    today = datetime.datetime.today()
                    scheduler_on = response_text['data']['device_settings']['relays'][0]['schedule_rules'][0]
                    scheduler_on = datetime.datetime.combine(today, datetime.datetime.strptime(scheduler_on[:4], '%H%M').time())
                    scheduler_off = response_text['data']['device_settings']['relays'][0]['schedule_rules'][1]
                    scheduler_off = datetime.datetime.combine(today, datetime.datetime.strptime(scheduler_off[:4], '%H%M').time())
                logger.info("Shelly settings returned.")
                return scheduler, scheduler_on, scheduler_off
            else:
                logger.error("Shelly API settings - not OK response.")
                raise Exception("Shelly API settings - not OK response.")
        except:
            logger.error("Shelly API settings - malformed response.")
            raise

    def call_api(url, timeout, response_handler, post=False, fields=None, auth=None, fallback_flag=False):
        """
        Call api.
        :param url:
        :param timeout:
        :param response_handler:
        :param post: if True, send post request
        :param fields: payload for post request
        :param fallback_flag: if True activate fallback in case of api call fail
        :return: vars returned by handler, retrieved from response
        """
        try:
            if auth is not None:
                url = url + urlencode(auth)
            if not post:
                response = http.request('GET', url, timeout=timeout, retries=False)
            else:
                http.clear()
                response = http.request('POST', url, timeout=timeout, retries=False, fields=fields)
            status_code = response.status
            if status_code == 200:
                handled = response_handler(response)
                return handled
            else:
                logger.error("API call error - non-200 status code returned: %s. Url: %s Detail: %s" %
                             (status_code, url, json.loads(response.data.decode('utf-8'))))
                raise Exception("API call error - non-200 status code returned: %s.  Url: %s" % (status_code, url))
        except:
            logger.exception("API call error - failed calling the endpoint. Url: %s" % url)
            if fallback_flag:
                fallback()
            raise

    ####################################
    # control logics
    try:
        ison, time_now = call_api(shelly_status_url, shelly_timeout, get_shelly_status, post=True, auth=auth)
    
        time.sleep(2)
        
        scheduler, scheduler_on, scheduler_off = call_api(shelly_settings_url, shelly_timeout, get_shelly_settings, post=True, auth=auth)
        
        
        def do_airq_part():
            logger.info("Air quality part.")           
            ssm = boto3.client('ssm')
            res = ssm.get_parameters(Names=['pm10_out_divin', 'pm25_out_divin'])
            time_now = datetime.datetime.now().replace(tzinfo=None)
            old_limit = datetime.timedelta(minutes=5)
            pm10 = [float(res['Parameters'][0]['Value'])]
            pm25 = [float(res['Parameters'][1]['Value'])]
            logger.info(f"Retrieved values {pm10}, {pm25}.")

            if (time_now - res['Parameters'][0]['LastModifiedDate'].replace(tzinfo=None) > old_limit) or (time_now- res['Parameters'][1]['LastModifiedDate'].replace(tzinfo=None) > old_limit):
                logger.error(f"PM parameters are too old {time_now - res['Parameters'][0]['LastModifiedDate'].replace(tzinfo=None)}.")
                fallback()
                return

            payload = auth.copy()
            payload["channel"] = "0"
        
            if all(i < pm10_limit_on for i in pm10) and all(i < pm25_limit_on for i in pm25):
                logger.info("Good air detected.")
                payload["turn"] = "on"
        
                if ison is None or not ison:
                    # non-ideal, there is 1 second limit between requests for shelly api
                    time.sleep(2)
                    logger.info("Turn on.")
                    call_api(shelly_switch_url, shelly_timeout, switch_handler, post=True, fields=payload, auth=auth)
            elif all(i >= pm10_limit_off for i in pm10) or all(i >= pm25_limit_off for i in pm25):
                logger.info("Bad air detected.")
                payload["turn"] = "off"
                if ison is None or ison:
                    # non-ideal, there is 1 second limit between requests for shelly api
                    time.sleep(2)
                    logger.info("Turn off.")
                    call_api(shelly_switch_url, shelly_timeout, switch_handler, post=True, fields=payload, auth=auth)
            else:
                logger.info("Air quality is in turnon-turnoff range.")
                    
        if scheduler:
            # logger.info(f"time_now: {time_now}")
            # logger.info(f"scheduler_on: {scheduler_on}")
            # logger.info(f"scheduler_off: {scheduler_off}")
            logger.info("Scheduler is on.")
            if time_now > scheduler_on and time_now < scheduler_off:
                logger.info("Scheduler is on and we are within operation hours.")
                do_airq_part()
            else:
                logger.info("Scheduler is on and we are outside operation hours.")
                if ison is None or ison:
                    # non-ideal, there is 1 second limit between requests for shelly api
                    time.sleep(2)
                    payload = auth.copy()
                    payload["channel"] = "0"
                    payload["turn"] = "off"
                    logger.info("Turn off.")
                    call_api(shelly_switch_url, shelly_timeout, switch_handler, post=True, fields=payload, auth=auth)
        else:
            logger.info("Scheduler is off.")
            do_airq_part()
            
    except:
        fallback()

    ####################################

    return {
        'statusCode': 200,
        'body': json.dumps('All good!')
    }
